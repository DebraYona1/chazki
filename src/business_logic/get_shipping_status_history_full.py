from src.config.logger import lambda_logger
from src.business_logic.sync_shipping_data import sync_shipping_data
from src.repositories.chazki.api import chazki_get_shipping_status_history_full


def get_shipping_status_history_full(track_code, store_code):
    lambda_logger.info("Getting full shipping status history from chazki")
    response = chazki_get_shipping_status_history_full(track_code, store_code)

    if response["response"] == 1:
        sync_shipping_data(track_code, store_code)
        return 1, response["statusList"]
    else:
        error_message = (
            response["messages"]
            if "messages" in response
            else response["deliveryResponse"]
        )

        lambda_logger.error(
            """Couldn't obtain shipping history data from chazki for track_code: """
            + track_code
        )
        lambda_logger.error("chazki error: " + error_message)
        return 0, error_message
