from src.config.logger import lambda_logger

from src.repositories.dynamo.models.shipping_info import ShippingInfo
from src.repositories.chazki.api import chazki_get_shipping_status_history_full


def sync_shipping_data(track_code, store_code):
    lambda_logger.info(
        """Starting sync operation for record with track_code: """ + track_code
    )

    status_data_response = chazki_get_shipping_status_history_full(
        track_code, store_code
    )

    if status_data_response["response"] == 1:
        status_data = status_data_response["statusList"]

        shipping_info_results = ShippingInfo.query(track_code, limit=1)

        try:
            shipping_info = shipping_info_results.__next__()

            shipping_info.refresh()

            shipping_info.update(
                actions=[ShippingInfo.status.set({"statusList": status_data})]
            )

            lambda_logger.info(
                """Succesfully synced record with track_code: """ + track_code
            )

            return 1, "DB_SYNCED"

        except:
            lambda_logger.warn("""Didn't found record for track_code: """ + track_code)
            lambda_logger.info("""Creating record for track_code: """ + track_code)

            shipping_info = ShippingInfo(
                track_code=track_code,
                delivery_code="None",
                store_code=store_code,
                status={"statusList": status_data},
            )

            shipping_info.save()

            lambda_logger.info(
                """Succesfully synced record with track_code: """ + track_code
            )

            return 1, "DB_SYNCED"

    else:
        lambda_logger.error(
            """Couldn't obtain shipping status data from chazki for track_code: """
            + track_code
        )
        return 0, """Couldn't obtain shipping status data from chazki"""
