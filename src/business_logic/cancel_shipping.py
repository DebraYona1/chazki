from src.config.logger import lambda_logger
from src.business_logic.sync_shipping_data import sync_shipping_data
from src.repositories.chazki.api import chazki_cancel_shipping


def cancel_shipping(track_code, store_code):
    lambda_logger.info("Cancelling shipping on chazki")
    response = chazki_cancel_shipping(track_code, store_code)

    if response["response"] == 1:
        sync_shipping_data(track_code, store_code)

        return 1, response["descriptionResponse"]
    else:
        lambda_logger.error("""Couldn't cancel shipping on chazki""")
        lambda_logger.error("chazki error: " + response["descriptionResponse"])
        return 0, response["descriptionResponse"]
