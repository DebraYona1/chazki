from src.config.logger import lambda_logger
from src.repositories.dynamo.models.shipping_info import ShippingInfo
from src.repositories.chazki.api import (
    chazki_get_shipping_status_history_full,
    chazki_request_shipping,
)


def get_initial_status(track_code, store_code):
    initial_status_data = chazki_get_shipping_status_history_full(
        track_code, store_code
    )

    response = (
        initial_status_data["statusList"]
        if initial_status_data["response"] == 1
        else []
    )

    return response


def request_shipping(track_code, shipping_data, store_code):
    lambda_logger.info("Creating shipping on chazki")
    shipping_request_response = chazki_request_shipping(shipping_data)

    if shipping_request_response["response"] == 1:
        initial_status_data = get_initial_status(track_code, store_code)

        lambda_logger.info(
            "Saving shipping initial data on dynamo for track_code: " "" + track_code
        )

        shipping_info = ShippingInfo(
            track_code=track_code,
            delivery_code=shipping_request_response["codeDelivery"],
            store_code=store_code,
            status={"statusList": initial_status_data},
        )

        shipping_info.save()

        lambda_logger.info(
            """Succesfully saved record with track_code: """ + track_code
        )

        return 1, {
            "codeDelivery": shipping_request_response["codeDelivery"],
            "trackCode": track_code,
            "statusList": initial_status_data,
        }
    else:
        lambda_logger.error("""Couldn't create shipping on chazki""")
        lambda_logger.error(
            "chazki error: " + shipping_request_response["codeDelivery"]
        )
        return 0, shipping_request_response["codeDelivery"]
