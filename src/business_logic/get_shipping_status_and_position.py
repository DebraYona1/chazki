from src.config.logger import lambda_logger
from src.business_logic.sync_shipping_data import sync_shipping_data
from src.repositories.chazki.api import chazki_get_shipping_status_and_position


def get_shipping_status_and_position(track_code, store_code):
    lambda_logger.info("Getting shipping status and position from chazki")
    response = chazki_get_shipping_status_and_position(track_code, store_code)

    if response["response"] == 1:
        sync_shipping_data(track_code, store_code)

        response.pop("descriptionResponse")
        response.pop("response")

        position = response["position"]
        response.pop("position")
        current_status = response

        return 1, {"position": position, "current_status": current_status}
    else:
        lambda_logger.error("""Couldn't get shipping status and position from chazki""")
        lambda_logger.error("chazki error: " + response["descriptionResponse"])
        return 0, response["descriptionResponse"]
