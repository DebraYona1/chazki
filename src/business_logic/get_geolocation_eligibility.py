from src.config.logger import lambda_logger
from src.repositories.chazki.api import chazki_get_geolocation_eligibility


def get_geolocation_eligibility(latitude, longitude, store_code):
    lambda_logger.info("Getting geolocation eligibility from chazki")
    response = chazki_get_geolocation_eligibility(latitude, longitude, store_code)

    if response["response"] == 1:
        return 1, response["descriptionResponse"]
    else:
        lambda_logger.error("""Couldn't get geolocation eligibility from chazki""")
        lambda_logger.error("chazki error: " + response["descriptionResponse"])
        return 0, response["descriptionResponse"]
