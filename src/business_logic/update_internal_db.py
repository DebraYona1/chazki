import os
from src.config.logger import lambda_logger
from datetime import datetime, timedelta, timezone
from src.repositories.chazki.api import chazki_get_shipping_status_history_full
from src.repositories.postgres.orders import (
    get_track_code_of_non_finished_orders_in_time_range,
    update_order_status,
)

STORE_CODE = os.getenv("STORE_CODE")


def update_internal_db():
    now = datetime.now(timezone(-timedelta(hours=5)))
    two_weeks_ago = now - timedelta(weeks=2)

    ids = get_track_code_of_non_finished_orders_in_time_range(two_weeks_ago, now)

    for id in ids:
        try:
            lambda_logger.info("Updating track code: " + id + " ....")

            status_data_response = chazki_get_shipping_status_history_full(
                id, STORE_CODE
            )

            if status_data_response["response"] == 1:

                if len(status_data_response["statusList"]) > 0:

                    last_status = status_data_response["statusList"].pop()

                    new_status = last_status["status"]

                    updated = update_order_status(id, new_status)

                    if updated < 1:
                        lambda_logger.info("No record for track code: " + id + " ....")

                else:
                    lambda_logger.info("No data for track code: " + id + " ....")
            else:
                lambda_logger.info(
                    "Failed getting info from chazki for track code: " + id
                )

        except:
            lambda_logger.info("Unexpected error for track code: " + id)
