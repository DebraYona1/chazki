import json
from src.config.logger import lambda_logger
from schema import SchemaError
from src.utils.format_response import json_format_response
from .schemas.request_shipping_schema import request_shipping_schema
from src.business_logic.request_shipping import request_shipping


def handler(event, context):
    lambda_logger.info("Initiating request_shipping function ....")
    try:
        body = json.loads(event["body"])

        input_data = request_shipping_schema.validate(body)

        track_code = input_data["deliveryTrackCode"]
        store_code = input_data["storeId"]

        ok, data = request_shipping(track_code, body, store_code)

        return {
            "statusCode": 200,
            "body": json_format_response(ok, data),
        }

    except SchemaError as e:
        lambda_logger.warn("BAD REQUEST : \n%s", str(e))
        return {"statusCode": 400, "body": json_format_response(0, str(e))}
    except:
        lambda_logger.exception("UNKNOWN  EXCEPTION: \n")
        return {"statusCode": 500, "body": json_format_response(0, "Unknown Error")}
