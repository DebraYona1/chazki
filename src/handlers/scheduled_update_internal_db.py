from src.business_logic.update_internal_db import update_internal_db
from src.config.logger import lambda_logger
from src.utils.format_response import json_format_response


def handler(event, context):
    lambda_logger.info("Initiating scheduled update internal db function ....")
    try:
        update_internal_db()

        lambda_logger.info("Scheduled update internal db function ran successfully")

    except:
        lambda_logger.exception("UNKNOWN  EXCEPTION: \n")
        return {"statusCode": 500, "body": json_format_response(0, "Unknown Error")}
