from src.config.logger import lambda_logger
from schema import SchemaError
from src.utils.format_response import json_format_response
from .schemas.get_geolocation_eligibility_schema import (
    get_geolocation_eligibility_schema,
)
from src.business_logic.get_geolocation_eligibility import get_geolocation_eligibility


def handler(event, context):
    lambda_logger.info("Initiating get_geolocation_eligibility function ....")
    try:
        query = event["queryStringParameters"]

        input_data = get_geolocation_eligibility_schema.validate(query)

        latitude = input_data["lat"]
        longitude = input_data["lng"]
        store_code = input_data["store"]

        ok, data = get_geolocation_eligibility(latitude, longitude, store_code)

        return {
            "statusCode": 200,
            "body": json_format_response(ok, data),
        }

    except SchemaError as e:
        lambda_logger.warn("BAD REQUEST : \n%s", str(e))
        return {"statusCode": 400, "body": json_format_response(0, str(e))}
    except:
        lambda_logger.exception("UNKNOWN  EXCEPTION: \n")
        return {"statusCode": 500, "body": json_format_response(0, "Unknown Error")}
