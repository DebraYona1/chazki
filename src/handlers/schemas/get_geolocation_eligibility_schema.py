from schema import Regex, Schema, And

get_geolocation_eligibility_schema = Schema(
    {
        "lat": And(
            str,
            Regex(
                r"^(\+|-)?(?:90(?:(?:\.0*)?)|(?:[0-9]|[1-8][0-9])(?:(?:\.[0-9]*)?))$"
            ),
            error="must provide a valid latitude number",
        ),
        "lng": And(
            str,
            Regex(
                r"^^(\+|-)?(?:180(?:(?:\.0*)?)|(?:[0-9]|[1-9][0-9]|1[0-7][0-9])(?:(?:\.[0-9]*)?))$"
            ),
            error="must provide a valid longitude number",
        ),
        "store": And(str, Regex(r"^\d{5}$"), error="store must have 5 digits"),
    }
)
