from schema import Regex, Schema, And

get_shipping_status_history_schema = Schema(
    {
        "trackCode": And(
            str, Regex(r"^\d{13}$"), error="trackCode must have 13 digits"
        ),
        "store": And(str, Regex(r"^\d{5}$"), error="store must have 5 digits"),
    }
)
