from schema import Const, Or, Regex, Schema, And, Use
from datetime import date, datetime

mode_options = [
    "REGULAR",
    "EXPRESS",
    "PROGRAMADO",
    "PROGRAMADO_1",
    "PROGRAMADO_2",
    "PROGRAMADO_3",
]
time_options = ["9-13", "13-17", "17-21"]
proof_payment_options = ["BOLETA", "FACTURA"]
payment_method_options = ["PAGADO", "EFECTIVO"]
country_options = ["PE", "MX", "AR"]
document_type_options = ["DNI", "RUC", "CDE"]
size_options = ["XS", "S", "M", "L"]


def verifyTimeAndMode(time_mode_dict):
    if time_mode_dict["mode"] == "PROGRAMADO":
        try:
            [shipping_date, shipping_time_range] = time_mode_dict["time"].split(" ")
            try:
                datetime.strptime(shipping_date, "%d/%m/%y").date()
            except:
                raise Exception("""date must have this format: 'dd/MM/YY NN-NN'""")
            if shipping_time_range not in time_options:
                raise Exception(
                    """'NN-NN' must be one of [ """ + " OR ".join(time_options) + " ]"
                )
        except ValueError:
            raise Exception(
                """If 'mode' is 'PROGRAMADO' then 'time' must have this format: 'dd/MM/YY NN-NN' where 'NN-NN' is one of [ """
                + " OR ".join(time_options)
                + " ]"
            )
    else:
        if len(time_mode_dict["time"]) > 0:
            raise Exception(
                """If 'mode' is not 'PROGRAMADO' then 'time' must be empty """
            )

    return True


request_shipping_schema = Schema(
    And(
        {
            "storeId": And(str, Regex(r"^\d{5}$"), error="storeId must have 5 digits"),
            "branchId": And(
                str, lambda x: len(x) > 0, error="branchId must not be empty"
            ),
            "deliveryTrackCode": And(
                str, Regex(r"^\d{13}$"), error="deliveryTrackCode must have 13 digits"
            ),
            "proofPayment": And(
                str,
                lambda x: x.upper() in proof_payment_options,
                error="proofPayment must be one of "
                + " OR ".join(proof_payment_options),
            ),
            "deliveryCost": Use(float, error="deliveryCost must be a number"),
            "mode": And(
                str,
                lambda x: x.upper() in mode_options,
                error="mode must be one of " + " OR ".join(mode_options),
            ),
            "time": And(str, error="time must be a string"),
            "paymentMethod": And(
                str,
                lambda x: x.upper() in payment_method_options,
                error="paymentMethod must be one of "
                + " OR ".join(payment_method_options),
            ),
            "country": And(
                str,
                lambda x: x.upper() in country_options,
                error="country must be one of " + " OR ".join(country_options),
            ),
            "listItemSold": [
                {
                    "name": And(
                        str, lambda x: len(x) > 0, error="name must not be empty"
                    ),
                    "currency": And(
                        str,
                        lambda x: len(x) == 3,
                        error="currency must be a string of length 3",
                    ),
                    "price": Use(float, error="price must be a number"),
                    "weight": Use(float, error="weight must be a number"),
                    "volumen": Use(float, error="volumen must be a number"),
                    "quantity": And(
                        int,
                        lambda x: x > 0,
                        error="quantity must be a integer greater than 0",
                    ),
                    "unity": And(
                        str, lambda x: len(x) > 0, error="unity must not be empty"
                    ),
                    "size": And(
                        str,
                        lambda x: x in size_options,
                        error="size must be one of " + " OR ".join(size_options),
                    ),
                }
            ],
            "notes": And(str, error="notes be a string"),
            "documentNumber": And(
                str, lambda x: len(x) > 0, error="documentNumber must not be empty"
            ),
            "name_tmp": And(str, error="name_tmp be a string"),
            "lastName": And(str, error="lastName be a string"),
            "companyName": And(str, error="companyName be a string"),
            "email": And(str, lambda x: len(x) > 0, error="email must not be empty"),
            "phone": And(str, lambda x: len(x) > 0, error="phone must not be empty"),
            "documentType": And(
                str,
                lambda x: x.upper() in document_type_options,
                error="documentType must be one of "
                + " OR ".join(document_type_options),
            ),
            "addressClient": And(
                Const(
                    Use(
                        lambda x: len(x) == 1,
                        error="addressClient must only have one address",
                    )
                ),
                [
                    {
                        "nivel_2": And(
                            str, lambda x: len(x) > 0, error="nivel_2 must not be empty"
                        ),
                        "nivel_3": And(
                            str, lambda x: len(x) > 0, error="nivel_3 must not be empty"
                        ),
                        "nivel_4": And(
                            str, lambda x: len(x) > 0, error="nivel_4 must not be empty"
                        ),
                        "name": And(
                            str, lambda x: len(x) > 0, error="name must not be empty"
                        ),
                        "reference": And(
                            str,
                            lambda x: len(x) > 0,
                            error="reference must not be empty",
                        ),
                        "alias": And(
                            str, lambda x: len(x) > 0, error="alias must not be empty"
                        ),
                        "position": {
                            "latitude": And(
                                Use(str),
                                Regex(
                                    r"^(\+|-)?(?:90(?:(?:\.0*)?)|(?:[0-9]|[1-8][0-9])(?:(?:\.[0-9]*)?))$"
                                ),
                            ),
                            "longitude": And(
                                Use(str),
                                Regex(
                                    r"^^(\+|-)?(?:180(?:(?:\.0*)?)|(?:[0-9]|[1-9][0-9]|1[0-7][0-9])(?:(?:\.[0-9]*)?))$"
                                ),
                            ),
                        },
                    }
                ],
            ),
        },
        Const(
            And(
                Schema({"mode": str, "time": str}, ignore_extra_keys=True),
                Use(verifyTimeAndMode),
            )
        ),
    )
)
