from src.config.logger import lambda_logger
from schema import SchemaError
from src.utils.format_response import json_format_response
from .schemas.cancel_shipping_schema import cancel_shipping_schema
from src.business_logic.cancel_shipping import cancel_shipping


def handler(event, context):
    lambda_logger.info("Initiating cancel_shipping function ....")
    try:
        query = event["queryStringParameters"]

        input_data = cancel_shipping_schema.validate(query)

        track_code = input_data["trackCode"]
        store_code = input_data["store"]

        ok, data = cancel_shipping(track_code, store_code)

        return {
            "statusCode": 200,
            "body": json_format_response(ok, data),
        }

    except SchemaError as e:
        lambda_logger.warn("BAD REQUEST : \n%s", str(e))
        return {"statusCode": 400, "body": json_format_response(0, str(e))}
    except:
        lambda_logger.exception("UNKNOWN  EXCEPTION: \n")
        return {"statusCode": 500, "body": json_format_response(0, "Unknown Error")}
