import json


def json_format_response(success=0, data="Unknown Error"):
    if success == 1:
        return json.dumps({"status": "SUCCESS", "data": data}, ensure_ascii=False)
    else:
        return json.dumps({"status": "FAILED", "message": data}, ensure_ascii=False)
