from .with_pg_connection import with_pg_connection


get_track_code_of_non_finished_orders_in_time_range_query = """
    select delivery_track_code 
    from orders_order 
    where delivery_type ='CHAZKI'
    and status_delivery <> 'COMPLETED' 
    and status_delivery <> 'FAILED_PICK'
    and created >= %s
    and created < %s
    """

update_order_status_query = """
    update orders_order set status_delivery = %s 
    where delivery_track_code = %s
    """


def get_track_code_of_non_finished_orders_in_time_range_fn_creator(from_date, to_date):
    def get_track_code_of_non_finished_orders_in_time_range_fn(cursor):
        cursor.execute(
            get_track_code_of_non_finished_orders_in_time_range_query,
            (from_date, to_date),
        )

        rows = cursor.fetchall()

        ids = []

        for row in rows:
            ids.append(row[0])

        return ids

    return get_track_code_of_non_finished_orders_in_time_range_fn


def get_track_code_of_non_finished_orders_in_time_range(from_date, to_date):
    return with_pg_connection(
        get_track_code_of_non_finished_orders_in_time_range_fn_creator(
            from_date, to_date
        )
    )


def update_order_status_fn_creator(track_code, new_status):
    def update_order_status_fn(cursor):
        cursor.execute(
            update_order_status_query,
            (track_code, new_status),
        )

        return cursor.rowcount

    return update_order_status_fn


def update_order_status(track_code, new_status):
    return with_pg_connection(update_order_status_fn_creator(track_code, new_status))
