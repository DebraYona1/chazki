import psycopg2
import os

DB_HOST = os.getenv("DB_HOST")
DB_USER = os.getenv("DB_USER")
DB_PASS = os.getenv("DB_PASS")


def with_pg_connection(fn):
    conn = None

    try:
        conn = psycopg2.connect(database=DB_HOST, user=DB_USER, password=DB_PASS)

        cursor = conn.cursor()

        return_value = fn(cursor)

        conn.commit()

        cursor.close()

        return return_value

    except psycopg2.DatabaseError as e:
        raise Exception("Database Error")

    except:
        raise Exception("Unknown exception in pg repository")

    finally:
        if conn:
            conn.close()