import requests
import json
from .helpers import CHAZKI_BASE_URL, CHAZKI_DEFAULT_HEADERS


def chazki_get_geolocation_eligibility(latitude, longitude, store_code):
    url = CHAZKI_BASE_URL + "/chazkiServices/delivery/coverage/position"
    params = {
        "store": store_code,
        "latitude": latitude,
        "longitude": longitude,
    }
    r = requests.get(url, params=params, headers=CHAZKI_DEFAULT_HEADERS)
    return r.json()


def chazki_request_shipping(payload):
    url = CHAZKI_BASE_URL + "/chazkiServices/delivery/create/deliveryService"
    r = requests.post(url, data=json.dumps([payload]), headers=CHAZKI_DEFAULT_HEADERS)
    return r.json()


def chazki_get_shipping_status_and_position(track_code, store_code):
    url = CHAZKI_BASE_URL + "/chazkiServices/track/select/deliveryCode"
    params = {"store": store_code, "code": track_code}
    r = requests.get(url, params=params, headers=CHAZKI_DEFAULT_HEADERS)
    return r.json()


def chazki_get_shipping_status_history(track_code, store_code):
    url = CHAZKI_BASE_URL + "/chazkiServices/delivery/status/record"
    params = {"store": store_code, "code": track_code}
    r = requests.get(url, params=params, headers=CHAZKI_DEFAULT_HEADERS)
    return r.json()


def chazki_get_shipping_status_history_full(track_code, store_code):
    url = CHAZKI_BASE_URL + "/chazkiServices/delivery/status/record/all"
    params = {"store": store_code, "code": track_code}
    r = requests.get(url, params=params, headers=CHAZKI_DEFAULT_HEADERS)
    return r.json()


def chazki_cancel_shipping(track_code, store_code):
    url = CHAZKI_BASE_URL + "/chazkiServices/delivery/cancel/order"
    params = {"store": store_code, "trackCode": track_code}
    r = requests.put(url, params=params, headers=CHAZKI_DEFAULT_HEADERS)
    return r.json()
