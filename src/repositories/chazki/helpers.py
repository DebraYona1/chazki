import os

CHAZKI_API_KEY = os.getenv("CHAZKI_API_KEY")
CHAZKI_BASE_URL = os.getenv("CHAZKI_BASE_URL")

CHAZKI_DEFAULT_HEADERS = {
    "content-type": "application/json",
    "chazki-api-key": CHAZKI_API_KEY,
}
