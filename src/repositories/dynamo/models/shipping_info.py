from pynamodb.models import Model
from pynamodb.attributes import UnicodeAttribute, MapAttribute

from src.config import IS_OFFLINE
from ..connection import SHIPPING_INFO_TABLE, LOCAL_DYNAMO_ENDPOINT


class ShippingInfo(Model):
    class Meta:
        table_name = SHIPPING_INFO_TABLE
        if IS_OFFLINE:
            host = LOCAL_DYNAMO_ENDPOINT

    track_code = UnicodeAttribute(hash_key=True)
    delivery_code = UnicodeAttribute(default="")
    store_code = UnicodeAttribute(default="")
    status = MapAttribute(default={})
