deploy:
	serverless deploy

prepare-local:
	pip install -r requirements/local.txt
	pip install -r requirements/base.txt
	serverless dynamodb install

local-dynamo:
	serverless dynamodb start

start-local:
	serverless offline

lint:
	flake8 src