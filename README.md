# CHAZKI INTEGRATION LAMBDAS

## LOCAL

- Create local virtual environment
- run `make prepare-local`
- on one terminal run `make local-dynamo`
- on another terminal run `make start-local`

## DEPLOY

- export AWS_PROFILE env
- run `make deploy`

## CHAZKI PITFALLS

Segun la documentacion [aqui](https://docs.chazki.com/#historial-de-estados) y [aqui](https://docs.chazki.com/#historial-de-estados-internos) el campo `descriptionResponse` como parte del payload de respuesta, pero en cambio el campo viene con el nombre `deliveryResponse`
